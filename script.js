var vue_app;

var requests_timeout = 60000;
var auto_refresh_delay = 10000;

var default_config = {
    data_version: 1,
    current_preset: 0,
    presets: [
        { name: "Default", schedules: {} }
    ]
};

schedules = {};

function get_ratp_url(kind, line, stop, dir) {
    var type = kind;

    switch (kind) {
    case "metro":
        type = "metros";
        break;
    case "tram":
        type = "tramways";
        break;
    case "rer":
        type = "rers";
        break;
    }

    return "https://makers.ratp.fr/api/schedules/"
        + type + "/" + line + "/" + stop + "/" + dir;
}

function get_times(line, stop_id, success, failure) {
    console.log("Getting time for line " + line.id + " " + stop_id);

    var stop = line.stops[stop_id];
    var url = get_ratp_url(line.kind, line.id, stop.name, stop.dir);

    if (stop.schedules === undefined) {
        Vue.set(stop, "schedules", []);

    }

    if (stop.state !== "auto-refresh") {
        stop.state = "refreshing";
    }

    $.ajax({
        url: url,
        timeout: requests_timeout,
    })
    .done(function (json) {
        var schedules = json["result"]["schedules"];
        var req_time = json["_metadata"]["date"];
        var time = req_time.split("T", 2)[1].split(":", 2).join(":")

        Vue.set(stop, "schedules", schedules);
        Vue.set(stop, "last_updated", time);
        if (stop.state !== "auto-refresh") {
            Vue.set(stop, "state", "updated");
        }

        if (success !== undefined) {
            success();
        }

        if (stop.state === "auto-refresh") {
            stop.auto_timer = setTimeout(function() {
                get_times(line, stop_id, success, failure);
            }, auto_refresh_delay);
        }
    })
    .fail(function (xhr, status, error) {
        console.log("error getting time");
        console.log(xhr.responseText);

        stop.state = "error";
        if (failure !== undefined) {
            failure();
        }
    });
}

function init_stop(kind, line_id, stop_id) {
    var line = schedules[kind][line_id];
    var stop = line.stops[stop_id];
    line.kind = kind;
    line.id = line_id;
    Vue.set(stop, "schedules", []);
    Vue.set(stop, "state", "created");
}

function init_bus_lines() {
    for (var kind in schedules) {
        var lines = schedules[kind];
        for (var line_id in lines) if (lines.hasOwnProperty(line_id)) {
            var line = lines[line_id];
            for (var i = 0; i < line.stops.length; i++) {
                init_stop(kind, line_id, i);
            }
        }
    }
}

function add_stop_message(message)
{
    $(".adder .message").text(message);
}

function add_stop(type, line, stop, dir)
{
    var url = get_ratp_url(type, line, stop, dir);
    $.get(url)
    .done(function () {
        var saved_schedules = get_schedules();

        if (saved_schedules[type] === undefined) {
            saved_schedules[type] = {};
            Vue.set(schedules, type, {});
        }
        if (saved_schedules[type][line] === undefined) {
            saved_schedules[type][line] = {stops: []};
            Vue.set(schedules[type], line, {stops: []});
        }
        saved_schedules[type][line]["stops"].push({name: stop, dir: dir});

        save_schedules(saved_schedules);

        var stop_id = schedules[type][line]["stops"].push({name: stop, dir: dir});
        stop_id = stop_id - 1;
        console.log("New stop id: " + stop_id);

        init_stop(type, line, stop_id);


        add_stop_message("New stop has been added.");
    })
    .fail(function () {
        add_stop_message("Error: cannot validate new stop information.");
    });
}

function add_stop_click()
{
    var type = $(".adder .type option:selected").val();
    var line = $(".adder .line").val();
    var stop = $(".adder .stop").val();
    var $dir = $(".adder .dir:checked");
    var dir = "";
    if ($dir.length === 1) {
        dir = $dir.val()
    } else if ($dir.length === 2) {
        dir = "A+R";
    }

    console.log("Try to add: type: " + type
        + ", line: " + line
        + ", stop: " + stop
        + ", dir: " + dir
    );

    add_stop(type, line, stop, dir);
}

function delete_stop(type, line, stop_id)
{
    var saved_schedules = get_schedules();
    var saved_stop_list = saved_schedules[type][line].stops;
    var stop_list = schedules[type][line].stops;

    saved_stop_list.splice(stop_id, 1);
    stop_list.splice(stop_id, 1);
    if (saved_stop_list.length === 0) {
        delete saved_schedules[type][line];
        delete schedules[type][line];
    }

    save_schedules(saved_schedules);

    console.log(type + "stop " + stop_id + " on line " + line + " deleted.");
}

function update_cookies()
{
    var json = docCookies.getItem("scheds");
    if (json === null) {
        console.log("No cookie stored.");
        return;
    }

    console.log("Cookie found, saving it in local storage and deleting it.");
    localStorage.setItem("scheds", json);
    docCookies.removeItem("scheds");
}

function update_data()
{
    var json = localStorage.getItem("scheds");
    if (json === null) {
        return;
    }

    console.log("Converting data to configuration version 1.");

    var data = JSON.parse(json);
    var conf = default_config;
    conf.presets[conf.current_preset].schedules = data;
    save_data(conf);
    localStorage.removeItem("scheds");
}

function get_data()
{
    update_cookies();
    update_data();

    var json = localStorage.getItem("data");
    if (json === null) {
        console.log("No data stored.");
        save_data(default_config);
        return default_config;
    }

    console.log("Data found with value: " + json + ".");
    var data = JSON.parse(json);

    return data;
}

function get_schedules()
{
    var data = get_data();
    return data.presets[data.current_preset].schedules;
}

function save_data(data)
{
    localStorage.setItem("data", JSON.stringify(data));
}

function save_schedules(schedules)
{
    var data = get_data();
    data.presets[data.current_preset].schedules = schedules;
    save_data(data);
}

function change_preset(preset)
{
    var data = get_data();
    if (data.presets.length <= preset) {
        console.error("Invalid preset " + preset);
        return;
    }

    data.current_preset = preset;
    save_data(data);

    location.reload();
}

function new_preset(name)
{
    var data = get_data();
    data.presets.push({name: name, schedules: {}});
    save_data(data);

    location.reload(); //TODO: remove
}

function download_conf(){
    var data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(get_data()));
    var node = document.createElement('a');
    node.setAttribute("href", data);
    node.setAttribute("download", "conf.json");
    document.body.appendChild(node); // required for firefox
    node.click();
    node.remove();
}

function read_load_conf(file) {
    var reader = new FileReader();

      reader.onload = function(e) {
          var content = e.target.result;
          var yesno = confirm("Do you want to load the new configuration? It will reset your current one.");
          if (yesno) {
              save_data(JSON.parse(content));
              location.reload();
          }
      };

      reader.readAsText(file);
}

function remove_preset(preset)
{
    var data = get_data();
    if (data.presets.length <= preset) {
        console.error("Invalid preset " + preset);
        return;
    }

    if (data.presets.length <= 1) {
        console.error("Cannot remove last preset.");
        return;
    }

    data.presets.splice(preset, 1);

    if (data.current_preset == preset) {
        data.current_preset = 0;
    } else if (data.current_preset > preset) {
        /* Removing a preset that is *before* in the list. */
        data.current_preset = Number(data.current_preset) - 1;
    }

    save_data(data);
    location.reload(); //TODO: put in the IF
}

function main() {
    schedules = get_schedules();

    $(".adder .add").click(add_stop_click);

    $(".reset").click(function() {
        var conf = confirm("All your configuration will be deleted. Are you sure?");
        if (conf === false) {
            return;
        }
        localStorage.removeItem("data");
        location.reload();
    });

    init_bus_lines();

    Vue.component("stop", {
        template: "#stop-template",
        props: {
            stop: Object,
            stop_id: Number,
            line: Object,
        },
        data: function() {
            return {
                sub_opened: false,
            }
        },
        methods: {
            refresh_clicked: function(event, line, stop_id) {
                get_times(line, stop_id, undefined, undefined);
                event.stopPropagation();
            },
            refresh_clicked_right: function(event, line, stop_id) {
                if (this.stop.state !== "auto-refresh") {
                    this.stop.state = "auto-refresh";
                    this.stop.auto_timer = setTimeout(function() {
                        get_times(line, stop_id, undefined, undefined);
                    }, auto_refresh_delay);
                } else {
                    clearTimeout(this.stop.auto_timer);
                    this.stop.state = "updated";
                }
                event.stopPropagation();
                event.preventDefault();
            },
            delete_stop: delete_stop,
            open_subtitle: function(event) {
                this.sub_opened = !(this.sub_opened);
            },
            refresh_b_enter: function(el) {
                switch (this.stop.state) {
                case "auto-refresh":
                    $(el).addClass("auto-refresh");
                    break;
                case "refreshing":
                    $(el).addClass("refreshing");
                    break;
                case "error":
                    $(el).addClass("refresh-error");
                    break;
                }
            },
        },
        mounted: function() {
            console.log("Getting initial time for line " + this.line.id + " " + this.stop_id);
            get_times(this.line, this.stop_id, undefined, undefined);
        }
    });

    new Vue({
        el: ".header .preset-select",
        data: {
            presets: get_data().presets,
            current_preset: get_data().current_preset,
        }
    });
    new Vue({
        el: "#burger .preset-select",
        data: {
            presets: get_data().presets,
            current_preset: get_data().current_preset,
        }
    });

    vue_app = new Vue({
        el: '#schedules',
        data: {
            transports: schedules,
        },
        methods: {
        },
        mounted: function() {
            $(".schedules .loading").remove();
        },
    });

    $(".header .menu-btn").click(function() {
        $("#burger").toggleClass("opened");
    });

    $(".header .preset-select").change(function() {
        var preset = $(".preset-select option:selected").val();
        change_preset(preset);
    });

    $(".preset-del").click(function() {
        var preset = $("#burger .preset-select option:selected").val();
        remove_preset(preset);
    });

    $(".preset-add").click(function() {
        var name = $(".preset-name").val();
        new_preset(name);
    });

    $(".dl-conf").click(function() {
        download_conf();
    });

    $(".up-conf-ok").click(function() {
        var file = $('.up-conf').get(0).files[0];
        read_load_conf(file);
    });
}

$(document).ready(main);
